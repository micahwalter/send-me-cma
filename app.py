from flask import Flask, request
from twilio.twiml.messaging_response import Body, Media, Message, MessagingResponse

import random
import requests

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():

    resp = MessagingResponse() 
    from_body = request.values.get('Body', None)
    body_strip = from_body.lower()

    ## Query the CMA API
    cma = "https://openaccess-api.clevelandart.org/api/artworks/?q=" + body_strip + "&has_image=1&cc0=1"
    rsp = requests.get(cma)

    json = rsp.json()
    artworks = json['data']
    random.shuffle(artworks)

    message = Message()

    url = artworks[0]['images']['web']['url']
    message.media(url)

    resp.append(message)

    return str(resp)

@app.route('/test', methods=['GET'])
def test():

    q = request.args.get('q')
    body_strip = q.lower()

    cma = "https://openaccess-api.clevelandart.org/api/artworks/?q=" + body_strip + "&has_image=1&cc0=1"
    rsp = requests.get(cma)

    json = rsp.json()
    artworks = json['data']
    random.shuffle(artworks)

    url = artworks[0]['images']['web']['url']
    html = '<img src="' + url + '" />'
    return(html)